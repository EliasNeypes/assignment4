Movie Web App
----------

name: Elias Neypes
eid: evn87
[Username]: EliasNeypes
[Jenkins DNS]: ec2-52-54-246-225.compute-1.amazonaws.com


This example shows deployment of a web application that uses MySQL backend.

Check GCP/steps.txt for how to deploy this application on Google Kubernetes Engine.
