import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie_table(ID INT UNSIGNED NOT NULL AUTO_INCREMENT, Year INT UNSIGNED, Title TEXT NOT NULL, ' \
                'Director TEXT, Actor TEXT, Release_Date TEXT, Rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor(buffered=True)

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    #cur = cnx.cursor()
    #(id INT UNSIGNED, year INT UNSIGNED, title, director, actor, release_date, rating, PRIMARY KEY (id))'
    #cur.execute("INSERT INTO movie_table () values ('Hello, World!')")
    #cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)

    cur.execute("SELECT title FROM movie_table")
    entries = [dict(title=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")
    #year = int(request.form['year'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    #rating = request.form['rating']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return hello(message = "Movie {} could not be inserted - {}<br>".format(title, exp))

    cur = cnx.cursor(buffered=True)

    try:
        stmt = "SELECT * FROM movie_table"
        cur.execute(stmt)
        results = cur.fetchall()
        id = None
        for movie in results:
            if movie[2].lower() == title.lower():
                id = movie[0]
                break
        if id is not None:
            return hello(message = "Movie with title {} already exists<br>".format(title))

        stmt = "INSERT INTO movie_table (Year, Title, Director, Actor, Release_Date, Rating) " \
            "values ({}, '{}', '{}', '{}', '{}', {})".format(year, title, director, actor, release_date, rating)
        cur.execute(stmt)
        cnx.commit()
    except Exception as exp:
        print("Error entering movie entry")
        return hello(message = "Movie {} could not be inserted - {}<br>".format(title, exp))

    return hello(message = "Movie {} successfully inserted<br>".format(title))

@app.route('/delete_movie', methods=['POST'])
def rmv_from_db():
    print("Received request.")
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        return hello(message = "Movie {} could not be deleted - {}<br>".format(title, exp))

    cur = cnx.cursor(buffered=True)

    try:
        stmt = "SELECT * FROM movie_table"
        cur.execute(stmt)
        results = cur.fetchall()
        id = None
        for movie in results:
            if movie[2].lower() == title.lower():
                id = movie[0]
                break
        if id is None:
            return hello("Movie with title {} does not exist<br>".format(title))
        stmt = "DELETE FROM movie_table WHERE " \
               "id = {}".format(id)
        cur.execute(stmt)
        cnx.commit()
    except Exception as exp:
        print("Error entering movie entry")
        return hello(message = "Movie {} could not deleted - {}<br>".format(title, exp))

    return hello(message = "Movie {} successfully deleted<br>".format(title))

@app.route('/update_movie', methods=['POST'])
def update_db():
    print("Recieved request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    try:
        stmt = "SELECT * FROM movie_table"
        cur.execute(stmt)
        results = cur.fetchall()
        id = None
        for movie in results:
            if movie[2].lower() == title.lower():
                id = movie[0]
                break
        if id is None:
            return hello(message = "Movie with title {} does not exist<br>".format(title))
        stmt = "UPDATE movie_table SET Year = {}, Title = '{}', Director ='{}', Actor ='{}', Release_date ='{}', Rating = {} " \
               "WHERE id ={}".format(year, title, director, actor, release_date, rating, id)
        cur.execute(stmt)
        cnx.commit()
    except Exception as exp:
        print("Error entering movie entry")
        return hello(message = "Movie {} could not updated - {}<br>".format(title, exp))
    return hello(message = "Movie {} successfully updated<br>".format(title))


@app.route('/search_movie' , methods=['POST'])
def actor_search():
    print("Recieved request.")

    search_actor = request.form.get('search_actor')

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    act_list = list()
    try:
        #LOCATE() is already case insensitive
        stmt = "SELECT * FROM movie_table WHERE LOCATE('{}', Actor) > 0".format(search_actor)
        cur.execute(stmt)
        results = cur.fetchall()
        count = 0

        for movie in results:
            count += 1
            record = "{}, {}, {}".format(movie[2], movie[1], movie[4])
            act_list.append(record)

        if count is 0:
            return hello(message = "No movies for actor {}".format(search_actor))

    except Exception as exp:
        print("Error searching actor")
        return hello(message = "Actor {} could not searched - {}<br>".format(search_actor, exp))
    return hello(message = "Results found", list = act_list)

@app.route('/highest_rating', methods=['GET'])
def get_highest_db():
    print("Recieved request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    high_list = list()
    try:
        stmt = "SELECT * FROM movie_table WHERE Rating = (SELECT MAX(Rating) from movie_table)"
        cur.execute(stmt)
        results = cur.fetchall()
        count = 0

        for movie in results:
            count += 1
            record = "{}, {}, {}, {}, {}".format(movie[2], movie[1], movie[4], movie[3], movie[6])
            high_list.append(record)

        if count is 0:
            return hello(message="No movies recorded")

    except Exception as exp:
        print("Error searching actor")
        return hello(message = "Highest rating could not searched - {}<br>".format(exp))
    return hello(message = "Results found", list = high_list)

@app.route('/lowest_rating', methods=['GET'])
def get_lowest_db():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    low_list = list()
    try:
        stmt = "SELECT * FROM movie_table WHERE Rating = (SELECT MIN(Rating) from movie_table)"
        cur.execute(stmt)
        results = cur.fetchall()
        count = 0

        for movie in results:
            count += 1
            record = "{}, {}, {}, {}, {}".format(movie[2], movie[1], movie[4], movie[3], movie[6])
            low_list.append(record)

        if count is 0:
            return hello(message="No movies recorded")

    except Exception as exp:
        print("Error searching actor")
        return hello(message="Lowest rating could not searched - {}<br>".format(exp))
    return hello(message= "Results found", list = low_list)


@app.route("/")
def hello(message = "", list = {}):
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', message=message, list=list)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
